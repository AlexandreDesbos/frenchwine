Domaine de La Romanée-Conti
Romanée-Saint-Vivant Grand Cru (Marey-Monge) 2015
Red wine

https://images.vivino.com/thumbs/qbolgWCtRq-z3tt-HeXRrA_pb_x600.png


Joseph Drouhin
Côte de Beaune 2017
Red wine

https://images.vivino.com/thumbs/IhsNCzgjRWehLkWpzOSRIA_pb_x600.png


Christian Moreau Pere & Fils
Chablis 1er Cru 'Vaillon' Cuvée 2015
White wine

https://images.vivino.com/thumbs/Eu17lHcoSjG4GoPvXQC4Ug_pb_x600.png

Ardhuy
Savigny Lès Beaune 'Clos des Godeaux' Blanc 2015
White wine

https://images.vivino.com/thumbs/5DeLrF6FQEKQTdbgG80jiQ_pb_x600.png


Château de Puligny Montrachet
Bourgogne Clos du Château 2010
White wine 

https://images.vivino.com/thumbs/JHpyu48PQbeCPmXVhr3gnA_pb_x600.png

J.M. Boillot
Mâcon-Villages 2015
White wine

https://images.vivino.com/thumbs/oOEwZFj0RdCkli3jTHw2zQ_pb_x600.png


Jean-Paul & Benoit Droin
Chablis 2017
White wine

https://images.vivino.com/thumbs/bNLMm4u6RBmHPBNkpwYlGg_pb_x600.png

Bret Brothers & The Soufrandière
La Soufrandiére Le Clos de Grand Pére Macon-Vinzelles 2016
White wine

https://images.vivino.com/thumbs/WhSsF47tQQSt2GoF-cJnmQ_pb_x600.png
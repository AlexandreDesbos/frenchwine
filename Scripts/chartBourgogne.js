
window.onload = function() {

        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            data: [{
                type: "pie",
                startAngle: 240,
                yValueFormatString: "##0.0\"%\"",
                indexLabel: "{label} {y}",
                dataPoints: [
                    {y: 51, label: "Chardonnay", color:"#ECD570"},
                    {y: 39.5, label: "Pinot Noir", color:"#69032F"},
                    {y: 2.5, label: "Gamay", color:"#543a69"},
                    {y: 6, label: "Aligoté", color:"#e5ec81"},
                    {y: 1, label: "Other"},
                ]
            }]
        });
        chart.render();
};



$(document).ready(function(){

    $('#touchMenu').hide();

    $('#dropDownBtn').click(function(event){
        event.stopPropagation();
        $("#dropDownContent").show(400);
    });

    $("body").click(function(){
        $("#dropDownContent").hide(400);
        $('#touchMenu').hide(400);
        $("#floating").show(400);

    });

    $('#floating').click(
        function(event) {
            event.stopPropagation();
            $('#floating').hide(200);
            $('#touchMenu').show(200);
        }
    );


});